package com.devcamp.task6070jparelationshipordercustomer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070jparelationshipordercustomer.model.CMenu;

public interface MenuRepository extends JpaRepository<CMenu, Long>{
    CMenu findById(int id);
    CMenu findByCombo(String combo);
}
